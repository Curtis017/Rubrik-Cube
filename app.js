var express = require('express');
var path = require('path');
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.use(express.static('js/dist'));
app.use(express.static('css'));

app.get('/', function (req, res) {
  res.render('index', { title: 'Express' });
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});
