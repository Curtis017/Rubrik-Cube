"use strict";

/**
 * @author Curtis Hughes / https://gitlab.com/Curtis017
 */

var RubrikCube = function () {

  // Graphic Content Variable (within scope)
  var GC = {
    elements: generateCubeCluster(),
    scene: new THREE.Scene(),
    raycaster: new THREE.Raycaster(),
    mouse: new THREE.Vector2(),
    pivot: new THREE.Object3D(),
    renderer: webglAvailable() ? new THREE.WebGLRenderer({ antialias: true, canvas: rubrik, alpha: true }) : new THREE.CanvasRenderer({ antialias: true, canvas: rubrik, alpha: true }),
    canvas: document.getElementById("rubrik"),
    rotationSpeed: 20,
    rotatePositive: true,
    rotationLock: false,
    cubesSelected: [],
    choice: 0,
    counter: 0,
    controls: null,
    camera: null,
    light: new THREE.AmbientLight(0xFFFFFF)
  };

  // setup initial values
  function init() {

    // setup camera
    GC.camera = new THREE.PerspectiveCamera(75, GC.canvas.clientWidth / GC.canvas.clientHeight, 0.1, 1000);
    GC.camera.position.x = 3;
    GC.camera.position.y = 3;
    GC.camera.position.z = 3;
    GC.camera.lookAt(new THREE.Vector3(0, 0, 0));

    // create scene
    GC.scene.add(GC.pivot);
    GC.elements.cubes.forEach(function (cube) {
      GC.scene.add(cube);
    });
    GC.elements.edges.forEach(function (edge) {
      GC.scene.add(edge);
    });

    // setup renderer
    GC.renderer.setSize(GC.canvas.clientWidth, GC.canvas.clientHeight, false);

    // Add 'OrbitControls' so that we can pan around with the mouse.
    GC.controls = new THREE.OrbitControls(GC.camera, GC.renderer.domElement);

    // render everything
    render();
  }

  // Main loop that keeps displaying objects
  function render() {
    requestAnimationFrame(render);
    rotate();

    GC.renderer.render(GC.scene, GC.camera);
    GC.controls.update();
  }

  // Rotates the object slowly
  function rotate() {
    if (GC.counter > 0) {
      // Select the current rotation direction
      GC.rotatePositive ? rotatePositiveAroundPivot() : rotateNegativeAroundPivot();

      // Decrement the counter
      GC.counter--;

      // Finished rotating
      if (GC.counter <= 0) {
        resetScene();
        GC.rotationLock = false;
      }
    }
  }

  // Creates cube geometry
  function createCubeMesh() {
    var materials = [new THREE.MeshBasicMaterial({ color: 0x009E60 }), // Green
    new THREE.MeshBasicMaterial({ color: 0xffffff }), // White
    new THREE.MeshBasicMaterial({ color: 0x0051BA }), // Blue
    new THREE.MeshBasicMaterial({ color: 0xC41E3A }), // Red
    new THREE.MeshBasicMaterial({ color: 0xFFD500 }), // Yellow
    new THREE.MeshBasicMaterial({ color: 0xFF5800 })];

    // Create the cube with the desired colors (above)
    var material = new THREE.MeshFaceMaterial(materials);
    var geometry = new THREE.BoxGeometry(1, 1, 1);
    var cube = new THREE.Mesh(geometry, material);
    return cube;
  }

  // Creates each indivual cube
  function generateCubeCluster() {
    var cubes = [];
    var edges = [];

    // Create cubes and set position
    for (var k = -1; k < 2; k++) {
      // z
      for (var i = -1; i < 2; i++) {
        // y
        for (var j = -1; j < 2; j++) {
          // x

          // Create objects
          var cube = createCubeMesh();
          var edge = new THREE.EdgesHelper(cube, 0x000000);

          // Set attributes
          edge.material.linewidth = 3;
          cube.position.x = j;
          cube.position.y = i;
          cube.position.z = k;

          // Add to the lists
          cubes.push(cube);
          edges.push(edge);
        }
      }
    }
    return { cubes: cubes, edges: edges };
  }

  // From Three.js website - detects if browser is WebGL capable (mobile)
  function webglAvailable() {
    try {
      var canvas = document.createElement("canvas");
      return !!(window.WebGLRenderingContext && (canvas.getContext("webgl") || canvas.getContext("experimental-webgl")));
    } catch (e) {
      return false;
    }
  }

  // Sets an array with the selected group of cubes
  function selectCubes(clickedCubePosition) {
    GC.scene.traverse(function (node) {
      if (node instanceof THREE.Mesh) {
        switch (GC.choice % 3) {
          case 0:
            if (Math.trunc(node.position.z) === Math.trunc(clickedCubePosition.z)) {
              GC.cubesSelected.push(node);
            }
            break;
          case 1:
            if (Math.trunc(node.position.x) === Math.trunc(clickedCubePosition.x)) {
              GC.cubesSelected.push(node);
            }
            break;
          case 2:
            if (Math.trunc(node.position.y) === Math.trunc(clickedCubePosition.y)) {
              GC.cubesSelected.push(node);
            }
            break;
        }
      }
    });

    // attaches selected group to the pivot
    GC.counter = GC.rotationSpeed;
    GC.pivot.rotation.set(0, 0, 0);
    GC.pivot.updateMatrixWorld();
    for (var i = 0; i < GC.cubesSelected.length; i++) {
      THREE.SceneUtils.attach(GC.cubesSelected[i], GC.scene, GC.pivot);
    }
  }

  // Rotates the pivot around the desired axis
  function rotatePositiveAroundPivot() {
    switch (GC.choice % 3) {
      case 0:
        GC.pivot.rotation.z += Math.PI / 2 / GC.rotationSpeed;
        break;
      case 1:
        GC.pivot.rotation.x += Math.PI / 2 / GC.rotationSpeed;
        break;
      case 2:
        GC.pivot.rotation.y += Math.PI / 2 / GC.rotationSpeed;
        break;
    }
  }

  // Rotates in the oposite of current direction
  function rotateNegativeAroundPivot() {
    switch (GC.choice % 3) {
      case 0:
        GC.pivot.rotation.z -= Math.PI / 2 / GC.rotationSpeed;
        break;
      case 1:
        GC.pivot.rotation.x -= Math.PI / 2 / GC.rotationSpeed;
        break;
      case 2:
        GC.pivot.rotation.y -= Math.PI / 2 / GC.rotationSpeed;
        break;
    }
  }

  // adds cubes back to the scene
  function resetScene() {
    GC.pivot.updateMatrixWorld();
    for (var i = 0; i < GC.cubesSelected.length; i++) {
      GC.cubesSelected[i].updateMatrixWorld();
      THREE.SceneUtils.detach(GC.cubesSelected[i], GC.pivot, GC.scene);
    }
    GC.cubesSelected = [];
  }

  // Changes direction of rotation
  function changeSet() {
    if (!GC.rotationLock) {
      GC.choice++;
    }
  }

  // changes rotation
  function changeRotation() {
    if (!GC.rotationLock) {
      GC.rotatePositive = GC.rotatePositive ? false : true;
    }
  }

  function cursorPosition(canvas, event) {
    var rect = canvas.getBoundingClientRect();
    var x = event.clientX - rect.left;
    var y = event.clientY - rect.top;
    return { x: x, y: y };
  }

  // main
  init();

  // Rotates selection of cubes
  $("#rubrik").on("click", function () {
    if (!GC.rotationLock) {

      // get the actual cursor position
      var curPos = cursorPosition(this, event);

      // Get the x and y coordinates of the mouse relative to the container
      GC.mouse.x = curPos.x / GC.canvas.offsetWidth * 2 - 1;
      GC.mouse.y = -(curPos.y / GC.canvas.offsetHeight) * 2 + 1;

      // update the picking ray with the camera and mouse position
      GC.raycaster.setFromCamera(GC.mouse, GC.camera);

      // calculate objects intersecting the picking ray
      var objects = [];
      GC.scene.traverse(function (node) {
        if (node instanceof THREE.Mesh) {
          objects.push(node);
        }
      });

      // set the cubes that are effected by the rotation
      var intersects = GC.raycaster.intersectObjects(objects);
      if (intersects.length > 0) {
        GC.rotationLock = true;
        selectCubes(intersects[0].object.position);
      }
    }
  });

  $(window).resize(function () {
    GC.camera.aspect = GC.canvas.clientWidth / GC.canvas.clientHeight;
    GC.renderer.setSize(GC.canvas.clientWidth, GC.canvas.clientHeight, false);
    GC.camera.updateProjectionMatrix();
  }, false);

  return {
    changeRotation: changeRotation,
    changeSet: changeSet
  };
}();